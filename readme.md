
# About project

This is a REST API for currency rates

## Backend

Backend is written using Node.js, Express.js, MongoDB database (Mongoose) and Typescript. 
To provide clean design and easier scalability for the future backend app implements repository pattern and manual dependency injection, DTOs in separate files and business logic in service class. 

CurrencyRepository takes care of quering data from database and returns entities defined in model. 

CurrencyService takes of mapping domain entities(ICurrency) to DTOs and vice versa (using CurrencyMapper) as well as using repository to access database.

Also api controller  CurrencyController was implemented that uses service for all business logic and is assigned to correspondng route.

Backend api doesn't yet implement correct error handling

### Functionality:
* Get all currencies by sending GET request to http://localhost:8001/currencies, data format:
```
{
    "count": 7,
    "currencies": [
        {
            "id": string,
            "name": string,
            "code": string,
            "rate": number,
            "sign": "string",
            "request": {
                "method": "GET",
                "url": link to single currency
            }
        }
    ]
}
```

* Get single currency by sending GET request to http://localhost:8001/currencies/id, data format:
```
{
    "id": string,
    "name": string,
    "code": string,
    "rate": number,
    "sign": "string"
}
```

* Create currency by sending POST request to http://localhost:8001/currencies/, data format:
```
{
    "name": string,
    "code": string,
    "rate": number,
    "sign": "string"
}
```
*Sign parameter is optional

* Update currency by sending PATCH request to http://localhost:8001/currencies/id, data format:
```
{
    "name": string?,
    "code": string?,
    "rate": number?,
    "sign": "string?"
}
```
* Delete currency by sending DELETE request to http://localhost:8001/currencies/id/


## Frontend

Frontend is written in vanilla JS with Vue.js framework. Vue.js app sends requests to backend api. Frontend is located in client folder. 

Frontend provides UI and functionality for
* viewing list of currencies
* viewing individual currencies
* creating new currencies
* editing and deleting existing currencies

Frontend app doesn't yet implement form validations and error handling

# Running

To run node server (on port 8001) first install packages with 
```
npm install
```
Then for development run command
```
npm run dev
```
And for production run
```
npm run prod
```


To run vue.js server first go to client directory and install packages with 
```
cd client
npm install
```
Then run
```
npm run serve
```
