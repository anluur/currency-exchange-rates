import Vue from "vue";
import VueRouter from "vue-router";
import CurrencyList from "../views/currency/CurrencyList.vue";
import CurrencyCreate from "../views/currency/CurrencyCreate.vue";
import CurrencyEdit from "../views/currency/CurrencyEdit.vue";
import CurrencyShow from "../views/currency/CurrencyShow.vue";
import CurrencyDelete from "../views/currency/CurrencyDelete.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: { name: "Currencies" }
  },
  {
    path: "/currencies",
    name: "Currencies",
    component: CurrencyList
  },
  {
    path: "/currencies/details/:id",
    name: "Currency",
    component: CurrencyShow,
    props: true
  },
  {
    path: "/currencies/create",
    name: "Create currency",
    component: CurrencyCreate,
    props: true
  },
  {
    path: "/currencies/edit/:id",
    name: "Edit currency",
    component: CurrencyEdit,
    props: true
  },
  {
    path: "/currencies/delete/:id",
    name: "Delete currency",
    component: CurrencyDelete,
    props: true
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
