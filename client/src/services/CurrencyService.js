import axios from 'axios';

export default class CurrencyService {



    static baseurl = 'http://localhost:8001/currencies/';

    static getAll() {
        return axios.get(this.baseurl)
            .then(response => {
                console.log(response);
                
                return response.data.currencies;
            })
            .catch(error => {
                console.warn(error);
                return [];
            });
    }

    static get(id) {
        return axios.get(this.baseurl + id)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return null;
            });
    }

    static create(item) {
        return axios.post(this.baseurl, JSON.stringify(item),  {headers: {'Content-Type': 'application/json'}})
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return JSON.stringify(error);
            });
    }

    static update(id, params) {

        return axios.patch(this.baseurl + id, JSON.stringify(params),  {headers: {'Content-Type': 'application/json'}})
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return JSON.stringify(error);
            });
    }

    static delete(id) {
        return axios.delete(this.baseurl + id)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                console.warn(error);
                return JSON.stringify(error);
            });
    }
}