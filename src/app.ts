import express, { Request, Response } from 'express';
const app = express();
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import CurrencyRoutes from './routes/CurrencyRoutes';
import cors from 'cors';
require('dotenv/config');

const port = process.env.PORT || 8001;

// Middlewares
app.use(cors());
app.use(bodyParser.json());

// Routes
app.use('/currencies',  new CurrencyRoutes().routes);
app.get('/', (req: Request, res: Response) => {
    res.status(302).redirect('/currencies');
});


// Connect to db
mongoose.connect(
    process.env.DB_CONNECTION!, 
    { useNewUrlParser: true, useUnifiedTopology: true }, 
    () => console.log('connected to database')
)

// Start listening
app.listen(port, () => {
    console.log("Now listening");
});