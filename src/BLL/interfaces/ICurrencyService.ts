import ICurrency from "../../models/interfaces/ICurrency";
import IOperationResponse from "../../DAL/interfaces/repositories/base/IOperationResponse";
import IAllCurrenciesDTO from "../DTOs/currency/IAllCurrenciesDTO";
import ICurrencyDTO from "../DTOs/currency/ICurrencyDTO";
import ICurrencyRepository from "../../DAL/interfaces/repositories/ICurrencyRepository";


export default interface ICurrencyService {

    getAll(): Promise<IAllCurrenciesDTO>;
    findById(id: string): Promise<ICurrencyDTO | null>;
    create(item: ICurrency): Promise<ICurrencyDTO>;
    update(id: string, params: Object): Promise<boolean>;
    delete(id: string): Promise<boolean>;
}