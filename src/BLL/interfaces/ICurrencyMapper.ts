import ICurrencyDTO from "../DTOs/currency/ICurrencyDTO";
import IAllCurrenciesDTO from "../DTOs/currency/IAllCurrenciesDTO";
import ICurrency from "../../models/interfaces/ICurrency";
import { ICurrencyListItemDTO } from "../DTOs/currency/ICurrencyListItemDTO";


export default interface ICurrencyMapper {
    singleToDTO(item: ICurrency): ICurrencyDTO;
    listItemToDTO(item: ICurrency): ICurrencyListItemDTO;
    allToDTO(items: ICurrency[]): IAllCurrenciesDTO;
}