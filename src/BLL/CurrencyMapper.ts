import ICurrency from "../models/interfaces/ICurrency";
import ICurrencyDTO from "./DTOs/currency/ICurrencyDTO";
import IAllCurrenciesDTO from "./DTOs/currency/IAllCurrenciesDTO";
import ICurrencyMapper from "./interfaces/ICurrencyMapper";
import url from 'url';
import { ICurrencyListItemDTO } from "./DTOs/currency/ICurrencyListItemDTO";

class CurrencyMapper implements ICurrencyMapper {

    public singleToDTO(item: ICurrency): ICurrencyDTO {
        const currencyDTO: ICurrencyDTO = {
            id: item.id,
            name: item.name,
            code: item.code,
            rate: item.rate.valueOf(),
            sign: item.sign
        }
        return currencyDTO;
    }

    public listItemToDTO(item: ICurrency): ICurrencyListItemDTO {
        const currencyDTO: ICurrencyListItemDTO = {
            id: item.id,
            name: item.name,
            code: item.code,
            rate: item.rate.valueOf(),
            sign: item.sign,
            request: {
                method: 'GET',
                url: 'http://localhost:8001/currencies/' + item.id
            }
        }
        return currencyDTO;
    }

    public allToDTO(items: ICurrency[]): IAllCurrenciesDTO {
        const currencyDTO: IAllCurrenciesDTO = {
            count: items.length,
            currencies: items.map(item => this.listItemToDTO(item))
        }
        return currencyDTO;
    }
}

Object.seal(CurrencyMapper);
export = CurrencyMapper;