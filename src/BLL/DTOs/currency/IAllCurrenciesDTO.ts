import ICurrencyDTO from "./ICurrencyDTO";


export default interface IAllCurrenciesDTO {
    count: number;
    currencies: ICurrencyDTO[];
}