import ICurrencyDTO from "./ICurrencyDTO";


export interface ICurrencyListItemDTO extends ICurrencyDTO {
    request: {
        method: string;
        url: string;
    };
}