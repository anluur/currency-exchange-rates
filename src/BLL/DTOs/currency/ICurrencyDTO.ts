
export default interface ICurrencyDTO {
    id: string;
    name: string;
    code: string;
    rate: number;
    sign: string | undefined;
}