import ICurrencyService from "./interfaces/ICurrencyService";
import ICurrencyMapper from "./interfaces/ICurrencyMapper";
import ICurrencyRepository from "../DAL/interfaces/repositories/ICurrencyRepository";
import IAllCurrenciesDTO from "./DTOs/currency/IAllCurrenciesDTO";
import ICurrencyDTO from "./DTOs/currency/ICurrencyDTO";
import ICurrency from "../models/interfaces/ICurrency";
import IOperationResponse from "../DAL/interfaces/repositories/base/IOperationResponse";


export class CurrencyService implements ICurrencyService {

    constructor(private mapper: ICurrencyMapper, private repository: ICurrencyRepository) {
    }

    public async getAll(): Promise<IAllCurrenciesDTO> {
        const currencies = await this.repository.getAll();
        return this.mapper.allToDTO(currencies);
    }

    public async findById(id: string): Promise<ICurrencyDTO | null> {
        const currency = await this.repository.findById(id);
        if (currency === null) {
            throw new Error(`Could not find currency with id ${id}`);
        }
        return this.mapper.singleToDTO(currency);
    }

    public async create(item: ICurrency): Promise<ICurrencyDTO> {
        const created = await this.repository.create(item);
        return this.mapper.singleToDTO(created);
    }

    public async update(id: string, params: Object): Promise<boolean> {
        const result = await this.repository.update(id, params);
        return this.operationSuccessful(result);
    }

    public async delete(id: string): Promise<boolean> {
        const result = await this.repository.delete(id);
        return this.operationSuccessful(result);
    }

    private operationSuccessful(response: IOperationResponse): boolean {
        if (response.ok === undefined) {
            return false;
        }
        return response.ok === 1;
    }
}
