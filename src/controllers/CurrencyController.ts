import { Request, Response } from 'express';
import Currency from '../models/CurrencySchema';
import IBaseController from './interfaces/IBaseController';
import ICurrencyService from '../BLL/interfaces/ICurrencyService';
import CurrencyRepository from '../DAL/repositories/CurrencyRepository';

class CurrencyController implements IBaseController {

    constructor(private service: ICurrencyService) {
    }

    public async getAll(req: Request, res: Response): Promise<void> {
        console.log('Get all currencies');        

        try {
            const result = await this.service.getAll();
            res.status(200).json(result);
        } catch (error) {
            res.status(500).json({
                message: error
            });
        }
    }

    public async findById(req: Request, res: Response): Promise<void> {
        console.log('Get a single currency');


        try {
            const result = await this.service.findById(req.params.id);
            if (result) {
                res.status(200).json(result);
            } else {
                res.status(404).json({ 
                    message: "No valid entry found" 
                });
            } 
        } catch (error) {
            res.status(500).json({
                message: error
            });
        }
    }

    public async create(req: Request, res: Response): Promise<void> {
        console.log('posting currency');

        try {
            const result = await this.service.create(req.body);
            res.status(200).json(result);
        } catch (error) {
            res.status(500).json({
                message: error
            });
        }
    }

    public async update(req: Request, res: Response): Promise<void> {
        console.log('updating currency');
        
        try {
            const result = await this.service.update(req.params.id, req.body);
            res.status(200).json({
                updateSuccessful: result
            });
        } catch (error) {
            res.status(400).json({
                message: error
            });
        }
    }

    public async delete(req: Request, res: Response): Promise<void> {
        console.log('deleting currency');

        try {
            const result = await this.service.delete(req.params.id);
            res.status(200).json({
                deleteSuccessful: result
            });
        } catch (error) {
            res.status(500).json({
                message: error
            });
        }
    }
}

Object.seal(CurrencyController);
export = CurrencyController;