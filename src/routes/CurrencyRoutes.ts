import express from 'express';
import CurrencyController from '../controllers/CurrencyController';
import CurrencyRepository from '../DAL/repositories/CurrencyRepository';
import { CurrencyService } from '../BLL/CurrencyService';
import CurrencyMapper from '../BLL/CurrencyMapper';

const router = express.Router();

class CurrencyRoutes {

    private currencyController: CurrencyController;

    constructor() {
        this.currencyController = new CurrencyController(
            new CurrencyService(
                new CurrencyMapper(),
                new CurrencyRepository()
            )
        );
    }

    get routes() {
        router.get('/', this.currencyController.getAll.bind(this.currencyController));
        router.get('/:id', this.currencyController.findById.bind(this.currencyController));
        router.post('/', this.currencyController.create.bind(this.currencyController));
        router.patch('/:id', this.currencyController.update.bind(this.currencyController));
        router.delete('/:id', this.currencyController.delete.bind(this.currencyController));

        return router;
    }
}

Object.seal(CurrencyRoutes);
export = CurrencyRoutes;