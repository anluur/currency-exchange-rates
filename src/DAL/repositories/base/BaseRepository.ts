import IBaseRepository from "../../interfaces/repositories/base/IBaseRepository";
import { Model, Document, FilterQuery } from "mongoose";
import IOperationResponse from "../../interfaces/repositories/base/IOperationResponse";

class BaseRepository<T extends Document> implements IBaseRepository<T> {
    
    constructor(private model: Model<T>) {
    }

    public async getAll(): Promise<T[]> {
        return await this.model.find();
    }

    public async findById(id: string): Promise<T | null> {
        return await this.model.findById(id);
    }
    public async create(item: T): Promise<T> {
        return await this.model.create(item);
    }
    public async update(id: string, params: Object): Promise<IOperationResponse> {
        return await this.model.updateOne({ _id: id } as FilterQuery<T>, { $set: params });
    }
    public async delete(id: string): Promise<IOperationResponse> {
        return await this.model.deleteOne({ _id: id } as FilterQuery<T>);
    }

}

interface DeleteResponse {
    ok?: number | undefined;
    n?: number | undefined;
    deletedCount?: number | undefined;
}

Object.seal(BaseRepository);
export = BaseRepository;