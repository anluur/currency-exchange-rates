import ICurrency from "../../models/interfaces/ICurrency";
import BaseRepository from "./base/BaseRepository";
import CurrencySchema from "../../models/CurrencySchema";
import ICurrencyRepository from "../interfaces/repositories/ICurrencyRepository";


class CurrencyRepository extends BaseRepository<ICurrency> implements ICurrencyRepository {

    constructor() {
        super(CurrencySchema)
    }
}

Object.seal(CurrencyRepository);
export = CurrencyRepository;