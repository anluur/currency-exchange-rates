import IBaseRepository from "./base/IBaseRepository";
import ICurrency from "../../../models/interfaces/ICurrency";


export default interface ICurrencyRepository extends IBaseRepository<ICurrency> {

}