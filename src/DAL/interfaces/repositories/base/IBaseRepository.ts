import { Document } from "mongoose";
import IOperationResponse from "./IOperationResponse";

export default interface IBaseRepository<T extends Document> {
    getAll(): Promise<T[]>;
    findById(id: string): Promise<T | null>;
    create(item: T): Promise<T>;
    update(id: string, params: Object): Promise<IOperationResponse>;
    delete(id: string): Promise<IOperationResponse>;
}
