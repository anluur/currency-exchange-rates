

export default interface IOperationResponse {
    ok?: number | undefined;
    [propName: string]: any;
}