import { Types, Document as Doc } from 'mongoose';

export default interface ICurrency extends Doc {
    _id: Types.ObjectId;
    name: string;
    code: string;
    rate: Number;
    sign: string | undefined;
}
