import mongoose, { Schema } from 'mongoose';
import ICurrency from './interfaces/ICurrency';

const CurrencySchema = new Schema({
    name: {
        type: String,
        required: [true, "Name field is required"]
    },
    code: {
        type: String,
        required: [true, "Code field is required"]
    },
    rate: {
        type: Number,
        required: [true, "Rate field is required"]
    },
    sign: {
        type: String,
        required: false
    },
})

export default mongoose.model<ICurrency>('Currencies', CurrencySchema);